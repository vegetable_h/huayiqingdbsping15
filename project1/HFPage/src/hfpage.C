#include <iostream>
#include <stdlib.h>
#include <memory.h>

#include "hfpage.h"
#include "heapfile.h"
#include "buf.h"
#include "db.h"


// **********************************************************
// page class constructor

void HFPage::init(PageId pageNo)
{
	curPage = pageNo;	
	slotCnt = 0;
	usedPtr = MAX_SPACE - DPFIXED;
	freeSpace = MAX_SPACE - DPFIXED + sizeof(slot_t); 
	prevPage = nextPage = INVALID_PAGE; 	
  // fill in the body
}

// **********************************************************
// dump page utlity
void HFPage::dumpPage()
{
    int i;

    cout << "dumpPage, this: " << this << endl;
    cout << "curPage= " << curPage << ", nextPage=" << nextPage << endl;
    cout << "usedPtr=" << usedPtr << ",  freeSpace=" << freeSpace
         << ", slotCnt=" << slotCnt << endl;
   
    for (i=0; i < slotCnt; i++) {
        cout << "slot["<< i <<"].offset=" << slot[i].offset
             << ", slot["<< i << "].length=" << slot[i].length << endl;
    }
}

// **********************************************************
PageId HFPage::getPrevPage()
{
	return prevPage;
}

// **********************************************************
void HFPage::setPrevPage(PageId pageNo)
{
	prevPage = pageNo;
}

// **********************************************************
void HFPage::setNextPage(PageId pageNo)
{
	nextPage = pageNo;
}

// **********************************************************
PageId HFPage::getNextPage()
{
	return nextPage;
}

// **********************************************************
// Add a new record to the page. Returns OK if everything went OK
// otherwise, returns DONE if sufficient space does not exist
// RID of the new record is returned via rid parameter.
Status HFPage::insertRecord(char* recPtr, int recLen, RID& rid)
{
    // fill in the body
	if (recLen + sizeof(slot_t) > freeSpace) return DONE;
	rid.pageNo = curPage; 
	int found = -1;
	for (int i = 0; i < slotCnt; i ++)
		if (slot[i].offset == -1) {found = i; break;}
	if (found != -1){
		rid.slotNo = found;
		slot[found].offset = usedPtr; slot[found].length = recLen; 
	}
	else{
		rid.slotNo = slotCnt; 	
		slot[slotCnt].offset = usedPtr; slot[slotCnt].length = recLen; 
		slotCnt ++;
		freeSpace -= sizeof(slot_t);
	}

	for (int i = usedPtr - recLen, j = 0; j < recLen; i ++, j ++){
		data[i] = *(recPtr + j); 
	}

	usedPtr -= recLen; 
	freeSpace -= recLen;

    return OK;
}

// **********************************************************
// Delete a record from a page. Returns OK if everything went okay.
// Compacts remaining records but leaves a hole in the slot array.
// Use memmove() rather than memcpy() as space may overlap.
Status HFPage::deleteRecord(const RID& rid)
{
    // fill in the body
	if (rid.pageNo != curPage) return FAIL;
	if (rid.slotNo >= slotCnt || rid.slotNo < 0) return FAIL;
	slot_t tmp = slot[rid.slotNo];
	if (tmp.offset == -1) return FAIL;
	for (int i = 0; i < slotCnt; i ++){
		if (slot[i].offset < tmp.offset && slot[i].offset != -1) slot[i].offset += tmp.length; 
	}
	usedPtr += tmp.length;
	for (int i = tmp.offset - 1; i >= usedPtr; i --)
		data[i] = data[i - tmp.length];
	freeSpace += tmp.length;
	slot[rid.slotNo].length = -1; slot[rid.slotNo].offset = -1;
	while (slot[slotCnt - 1].offset == -1 && slotCnt > 0){
		slotCnt --; freeSpace += sizeof(slot_t);
	}
    return OK;
}

// **********************************************************
// returns RID of first record on page
Status HFPage::firstRecord(RID& firstRid)
{
	int maxOffset = 0;
	int found = 0;
	for (int i = 0; i < slotCnt; i ++)
		if (slot[i].offset > maxOffset){
			maxOffset = slot[i].offset;
			found = 1; firstRid.pageNo = curPage; firstRid.slotNo = i;
		}
	if (found == 1) return OK;
		else return DONE;
}

// **********************************************************
// returns RID of next record on the page
// returns DONE if no more records exist on the page; otherwise OK
// !!!!!!!!!!!!!!!! FAIL or DONE?
Status HFPage::nextRecord (RID curRid, RID& nextRid)
{
    // fill in the body
	if (curRid.pageNo != curPage) return FAIL;
	if (curRid.slotNo >= slotCnt || curRid.slotNo < 0) return FAIL;
	if (slot[curRid.slotNo].offset == -1) return FAIL;
	int ansOffset = slot[curRid.slotNo].offset - slot[curRid.slotNo].length;
	int found = 0;
	for (int i = 0; i < slotCnt; i ++)
		if (slot[i].offset == ansOffset){
			found = 1; nextRid.pageNo = curPage; nextRid.slotNo = i;
		}
	if (found == 1) return OK;
		else return DONE;
}

// **********************************************************
// returns length and copies out record with RID rid
Status HFPage::getRecord(RID rid, char* recPtr, int& recLen)
{
    // fill in the body
	if (rid.pageNo != curPage) {return FAIL; /*cerr<<"curPage:"<<curPage<<endl;*/}
	if (rid.slotNo >= slotCnt || rid.slotNo < 0) {return FAIL; /*cerr<<"slotCnt:"<<slotCnt<<endl;*/}
	if (slot[rid.slotNo].offset == -1) {return FAIL; /*cerr<<"offset"<<endl;*/}
	recLen = slot[rid.slotNo].length;
	for (int i = slot[rid.slotNo].offset - slot[rid.slotNo].length, j = 0; j < recLen; i ++, j ++)
		*(recPtr + j) = data[i];
    return OK;
}

// **********************************************************
// returns length and pointer to record with RID rid.  The difference
// between this and getRecord is that getRecord copies out the record
// into recPtr, while this function returns a pointer to the record
// in recPtr.
Status HFPage::returnRecord(RID rid, char*& recPtr, int& recLen)
{
    // fill in the body
	if (rid.pageNo != curPage) {return FAIL; /*cerr<<"curPage:"<<curPage<<endl;*/}
	if (rid.slotNo >= slotCnt || rid.slotNo < 0) {return FAIL; /*cerr<<"slotCnt:"<<slotCnt<<endl;*/}
	if (slot[rid.slotNo].offset == -1) {return FAIL; /*cerr<<"offset"<<endl;*/}

	recLen = slot[rid.slotNo].length;
	recPtr = &data[slot[rid.slotNo].offset - slot[rid.slotNo].length];
    return OK;
}

// **********************************************************
// Returns the amount of available space on the heap file page
int HFPage::available_space(void)
{
    return freeSpace - sizeof(slot_t);
}

// **********************************************************
// Returns 1 if the HFPage is empty, and 0 otherwise.
// It scans the slot directory looking for a non-empty slot.
bool HFPage::empty(void)
{
	for (int i = 0; i < slotCnt; i ++)
		if (slot[i].offset != -1) return false;
    return true;
}



