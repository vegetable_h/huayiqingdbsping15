/*
 * btindex_page.cc - implementation of class BTIndexPage
 *
 */

#include "btindex_page.h"

// Define your Error Messge here
const char* BTIndexErrorMsgs[] = {
};
static error_string_table btree_table(BTINDEXPAGE, BTIndexErrorMsgs);

Status BTIndexPage::insertKey (const void *key,
                               AttrType key_type,
                               PageId pageNo,
                               RID& rid)
{
	KeyDataEntry* target = new KeyDataEntry();	
	int* recLen = new int();
	Datatype data; data.pageNo = pageNo;
	make_entry(target, key_type, key, INDEX, data, recLen);
	Status status = SortedPage::insertRecord(key_type, (char*)target, *recLen, rid);
  return status;
}

Status BTIndexPage::deleteKey (const void *key, AttrType key_type, RID& curRid)
{
	return SortedPage::deleteRecord(curRid);
}

Status BTIndexPage::get_page_no(const void *key,
                                AttrType key_type,
                                PageId & pageNo,
								int key_size)
{
	RID cur, prev; 		
	Status status;
	status = HFPage::firstRecord(cur);
	if (status != OK) return status;
	char* rec = new char(sizeof(Datatype));
	int recLen;
	status = HFPage::getRecord(cur, rec, recLen);
	if (status != OK) return status;
	void* curKey = (void*)malloc(key_size);
	Datatype* curData = new Datatype();
	get_key_data(curKey, curData, (KeyDataEntry*)rec, recLen, INDEX);
	if (keyCompare(key, curKey, key_type) < 0)	
		pageNo = getLeftLink();
	else{
		prev = cur;
		pageNo = curData->pageNo;
		void* lastKey = curKey;
		//cout<<"begin"<<endl;
		while (keyCompare(key, curKey, key_type)>= 0){
			pageNo = curData->pageNo;
			//cout<<((Keytype*)key)->intkey<<","<<((Keytype*)curKey)->intkey<<endl;
			status = HFPage::nextRecord(prev, cur);
			if (status != OK) break;
			status = HFPage::getRecord(cur, rec, recLen);
			if (status != OK) return status;
			get_key_data(curKey, curData, (KeyDataEntry*)rec, recLen, INDEX);
			prev = cur;
			lastKey = curKey;
		}
		//cout<<"end"<<endl;
	} 
	delete rec;
	delete curData;
	free(curKey);
  return OK;
}

    
Status BTIndexPage::get_first(RID& rid,
                              void *key,
                              PageId & pageNo)
{
	Status status;
	status = HFPage::firstRecord(rid);
	if (status != OK) return status;
	char* rec = new char(sizeof(Datatype));
	int recLen;
	status = HFPage::getRecord(rid, rec, recLen);
	if (status != OK) return status;
	Datatype* data = new Datatype();
	get_key_data(key, data, (KeyDataEntry*)rec, recLen, INDEX);
	pageNo = data->pageNo;
	delete rec;
	delete data;
	return OK;
}

Status BTIndexPage::get_next(RID& rid, void *key, PageId & pageNo)
{
	Status status;
	RID prev = rid;
	status = HFPage::nextRecord(prev, rid);
	if (status != OK) return status;
	char* rec = new char(sizeof(Datatype));
	int recLen;
	status = HFPage::getRecord(rid, rec, recLen);
	if (status != OK) return status;
	Datatype* data = new Datatype();
	get_key_data(key, data, (KeyDataEntry*)rec, recLen, INDEX);
	pageNo = data->pageNo;
	delete rec;
	delete data;
	return OK;
}

bool BTIndexPage::isFull(const void* key, AttrType key_type){
	return get_key_data_length(key, key_type, INDEX) > HFPage::available_space(); 
} 

void BTIndexPage::getKey(void* key){
	PageId pId;
	RID rid;
	get_first(rid, key, pId);
}
